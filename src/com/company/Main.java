package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);

        Integer menu;
        boolean condicao = true;
        List<Conta> contas = new ArrayList<>();

        do{
            System.out.println("      - SISTEMA BANCÁRIO -");
            System.out.println(" 1 - Criar Conta");
            System.out.println(" 2 - Realizar transações");
            System.out.println(" 3 - Sair do sistema");
            menu = ler.nextInt();

            switch (menu) {
                case 1:
                    System.out.println("    - CRIAÇÃO DE CONTA - ");
                    Conta conta = new Conta();
                    contas.add(conta);
                    System.out.println("\n");
                    break;

                case 2:
                    Transacoes transacoes = new Transacoes(contas);
                    break;

                case 3:
                    condicao = false;
                    break;

                default:
                    System.out.println("Opção não encontrada!");
                    break;
            }
        }
        while (condicao);

    }
}
