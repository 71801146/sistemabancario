package com.company;

import java.util.List;
import java.util.Scanner;

public class Transacoes {
    Scanner ler = new Scanner(System.in);

    public Transacoes(List<Conta> contaList) {
        boolean condicao = true;
        int menu;

        do {
            System.out.println("    - TRANSAÇÕES - ");
            System.out.println(" 1 - Saque");
            System.out.println(" 2 - Transferência");
            System.out.println(" 3 - Depósito");
            System.out.println(" 4 - Voltar ao menu");
            menu = ler.nextInt();

            switch (menu) {
                case 1: //SAQUE
                    sacar(contaList);
                    break;

                case 2: //TRANSFERENCIA
                    transferir(contaList);
                    break;

                case 3: //DEPÓSITO
                    depositar(contaList);
                    break;

                case 4: //VOLTAR AO MENU
                    condicao = false;
                    System.out.println("\n");
                    break;

                default:
                    System.out.println("Opção não encontrada!");
                    break;
            }
        }
        while (condicao);
    }

    public Integer conta(List<Conta> contaList) {
        int conta;
        Integer index = null;
        System.out.print("Digite o numero da conta: ");
        conta = ler.nextInt();

        for (int i = 0; i < contaList.size(); i++) {
            if (contaList.get(i).getConta().equals(conta)) {
                index = i;
                i = contaList.size();
            }
        }

        return index;
    }

    public void sacar(List<Conta> contaList) {
        Double valor;
        Integer conta;

        conta = conta(contaList);

        if (conta != null)
        {
            Conta c1 = contaList.get(conta);
            System.out.println("Conta do tipo " + c1.getTipoConta() + " no nome de " + c1.getCliente() + ". Saldo R$ " + c1.getSaldo());
            System.out.print("Digite o valor a ser sacado: ");
            valor = ler.nextDouble();

            if (c1.getSaldo() >= valor) {
                c1.setSaldo(c1.getSaldo() - valor);

                System.out.println("Valor R$ " + valor + " sacado com sucesso, saldo atual R$ " + c1.getSaldo());
            }
            else {
                System.out.println("Valor não disponível na conta, saldo atual R$ " + c1.getSaldo());
            }
            System.out.println("\n");
        }
        else {
            System.out.println("Conta não existe! \n");
        }

    }

    public void transferir(List<Conta> contaList) {
        Integer contaOrigem, contaDestino;
        Double valor;

        System.out.println("Conta Origem");
        contaOrigem = conta(contaList);
        System.out.println("Conta Destino");
        contaDestino = conta(contaList);

        if (contaOrigem != null && contaDestino != null) {
            Conta cOrigem = contaList.get(contaOrigem);
            System.out.println("Conta origem do tipo " + cOrigem.getTipoConta() + " no nome de " + cOrigem.getCliente() + ". Saldo R$ " + cOrigem.getSaldo());

            Conta cDestino = contaList.get(contaDestino);
            System.out.println("Conta destino do tipo " + cDestino.getTipoConta() + " no nome de " + cDestino.getCliente() + ". Saldo R$ " + cDestino.getSaldo());

            System.out.print("Digite o valor a ser transferido: ");
            valor = ler.nextDouble();

            if (cOrigem.getSaldo() >= valor) {
                cOrigem.setSaldo(cOrigem.getSaldo() - valor);
                cDestino.setSaldo(cDestino.getSaldo() + valor);

                System.out.println("Valor de R$ " + valor + " transferido da conta " + cOrigem.getConta() + " para a conta " + cDestino.getConta());
                System.out.println("Sado atual na conta " + cOrigem.getConta() + " é de R$ " + cOrigem.getSaldo());
                System.out.println("Sado atual na conta " + cDestino.getConta() + " é de R$ " + cDestino.getSaldo());
            }
            else {
                System.out.println("Valor não disponível na conta, saldo atual é de R$ " + cOrigem.getSaldo());
            }
            System.out.println("\n");
        }
        else {
            System.out.println("Conta não existe! \n");
        }

    }

    public void depositar(List<Conta> contaList) {
        Integer contaOrigem;
        Double valor;

        contaOrigem =conta(contaList);

        if (contaOrigem != null) {
            Conta cOrigem = contaList.get(contaOrigem);
            System.out.println("Conta do tipo " + cOrigem.getTipoConta() + " no nome de " + cOrigem.getCliente() + ". Saldo R$ " + cOrigem.getSaldo());

            System.out.print("Digite o valor a ser depositado: ");
            valor = ler.nextDouble();

            cOrigem.setSaldo(cOrigem.getSaldo() + valor);

            System.out.println("Valor R$ " + valor + " depositado com sucesso na conta " + cOrigem.getConta() + ". Saldo atual de R$ " + cOrigem.getSaldo());

            System.out.println("\n");
        }
        else {
            System.out.println("Conta não existe! \n");
        }
    }
}
