package com.company;

import java.util.Random;
import java.util.Scanner;

public class Conta {
	Scanner ler = new Scanner(System.in);
	Random numeroRandom = new Random();
	private Integer conta;
	private String cliente, tipoConta;
	private Double saldo;

	public Conta() {
		setSaldo(1000.0);

		System.out.print("Digite o seu nome: ");
		setCliente(ler.next().toUpperCase());

		System.out.print("Escolhe entre conta poupança P ou conta corrente C -> ");
		setTipoConta(ler.next().toUpperCase());

		if (tipoConta.equals("P")) {
			setConta(numeroRandom.nextInt((9999 - 1000) + 1) + 1000); // APENAS 4 DIGITOS
			System.out.println(" Conta poupança criada: " + getConta());
		}
		else {
			setConta(numeroRandom.nextInt((999999 - 100000) + 1) + 100000); // APENAS 6 DIGITOS
			System.out.println(" Conta corrente criada: " + getConta());
		}
	}

	public Integer getConta() {
		return conta;
	}

	public void setConta(Integer conta) {
		this.conta = conta;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
}
